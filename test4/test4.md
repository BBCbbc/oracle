# 实验4：PL/SQL语言打印杨辉三角

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

*   认真阅读并运行下面的杨辉三角源代码。
*   将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。
*   运行这个存储过程即可以打印出N行杨辉三角。
*   写出创建YHTriangle的SQL语句。

## 杨辉三角源代码

```sql

--在sqldevelope中创建一个可以传入参数的存储过程
--参数命名为p_rows，每次通过p_rows向存储过程中进行参数的传递
CREATE OR REPLACE PROCEDURE YHTriangles(p_rows IN INTEGER) AS
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := p_rows; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/

--根据意愿传入想要的参数，比如12
set serveroutput on
begin
YHTriangles(12);
end;

--

```

## 步骤

1.定义一个名为 t\_number 的 VARRAY 数组类型，用于存储杨辉三角的每一行数据。 定义存储过程 YHTriangles，并在其中声明变量和数组。

2.打印杨辉三角的第一行和第二行的数据。

3.使用循环语句和条件语句，计算出每一行的数据，并将其存储到数组中。

4.使用循环语句和条件语句，打印出整个杨辉三角的数据。

5.最后，在 oracle块中调用存储过程，并传入一个整数参数，即可打印出指定行数的杨辉三角。 总的来说，这段代码通过实现一个杨辉三角的存储过程，让我们了解了如何在 SQL Developer 中创建带有参数的存储过程，并使用 VARRAY 数组、循环语句和条件语句实现了一个数学算法。



## 实验注意事项

*   完成时间：2023-05-15，请按时完成实验，过时扣分。
*   查询语句及分析文档`必须提交`到：你的oracle项目中的test4目录中。
*   实验分析及结果文档说明书用Markdown格式编写。

## 对实验的分析与总结

这是一个oracle存储过程，用于打印杨辉三角形，打印的行数由参数 p\_rows 指定。
该存储过程使用了 VARRAY 类型的数组 rowArray 来存储每一行的数字。在初始化数组后，代码通过循环计算并打印每一行的数字，其中第 1 行和第 2 行需要手动打印。
在计算每一行的数字时，代码使用了类似动态规划的思想，通过上一行的数字计算得到当前行的数字。具体来说，代码用一个循环计算出当前行除了第 1 个和最后一个数字以外的其他数字，然后在打印时添加上第 1 个和最后一个数字。
本次实验是通过分析一个 orcale存储过程的代码，来了解基本语法和数组类型的使用。
在实验中，我们看到了orcale中 VARRAY 类型的定义和使用，以及存储过程的基本结构和语法。我们还学习了如何使用循环和条件语句来实现算法，并使用 dbms\_output 函数来输出结果。
总的来说，本次实验让我们了解了oracle基本知识和语法，以及如何使用 oracle来实现一些简单的算法。同时，通过分析代码，我们也学会了如何阅读和理解他人编写的 oracle代码。

