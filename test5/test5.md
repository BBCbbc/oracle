# 实验5：包，过程，函数的用法

## 实验目的

*   了解PL/SQL语言结构
*   了解PL/SQL变量和常量的声明和使用方法
*   学习包，过程，函数的用法。

## 实验内容

*   以hr用户登录

1.  创建一个包(Package)，包名是MyPack。
2.  在MyPack中创建一个函数Get\_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3.  在MyPack中创建一个过程GET\_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
    Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID



```

# **`实现步骤`**


1.创建一个包(Package)，包名是MyPack
\--对存储包直接使用右键创建一个名为MyPack的包

```sql

create or replace PACKAGE MYPACK AS 
  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
END MYPACK;
```

2.在MyPack中创建一个函数Get\_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。

```sql

CREATE OR REPLACE PROCEDURE GET_EMPLOYEES(p_employee_id IN INTEGER) IS
  CURSOR c_emp IS
    SELECT employee_id, first_name, last_name, manager_id
    FROM employees
    START WITH employee_id = p_employee_id
    CONNECT BY PRIOR employee_id = manager_id;
BEGIN
  FOR emp IN c_emp LOOP
    dbms_output.put_line('Employee ID: ' || emp.employee_id || ', Name: ' || emp.first_name || ' ' || emp.last_name);
  END LOOP;
END;
/
```


代码分析：
该过程接受一个员工 ID 参数 p\_employee\_id，并使用递归查询方式查询该员工及其所有下属、子下属员工的信息。
在过程体中，我们首先定义了一个名为 c\_emp 的游标，用于查询指定员工及其所有下属、子下属员工的信息。我们使用 START WITH 和 CONNECT BY PRIOR 子句来指定递归查询的条件。
接着，我们使用一个 FOR 循环遍历游标中的每一行记录，并使用 dbms\_output 函数输出员工的 ID 和姓名信息。

3.在MyPack中创建一个过程GET\_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。

```

CREATE OR REPLACE PROCEDURE GET_EMPLOYEES(p_employee_id IN INTEGER) IS
  CURSOR c_emp IS
    SELECT LEVEL, employee_id, first_name, manager_id
    FROM employees
    START WITH employee_id = p_employee_id
    CONNECT BY PRIOR employee_id = manager_id;
BEGIN
  FOR emp IN c_emp LOOP
    dbms_output.put_line('Level: ' || emp.level || ', Employee ID: ' || emp.employee_id || ', Name: ' || emp.first_name);
  END LOOP;
END;
/
```


代码分析
该过程接受一个员工 ID 参数 p\_employee\_id，并使用递归查询方式查询该员工及其所有下属、子下属员工的信息。
在过程体中，我们使用 SELECT 语句查询指定员工及其所有下属、子下属员工的信息，并使用 LEVEL 关键字获取当前员工在组织架构中的层级。
接着，我们使用一个 FOR 循环遍历游标中的每一行记录，并使用 dbms\_output 函数输出员工的层级、ID 和姓名信息。

## 实验注意事项，完成时间： 2023-05-16日前上交

*   请按时完成实验，过时扣分。
*   查询语句及分析文档`必须提交`到：你的Oracle项目中的test5目录中。
*   上交后，通过这个地址应该可以打开你的源码：<https://github.com/你的用户名/oracle/tree/master/test5>
*   实验分析及结果文档说明书用Markdown格式编写。

## 脚本代码参考

```sql
create or replace PACKAGE MyPack IS
/*
本实验以实验4为基础。
包MyPack中有：
一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，
一个过程:Get_Employees(V_EMPLOYEE_ID NUMBER)
*/
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/

create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```

## 测试

```sh

函数Get_SalaryAmount()测试方法：
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

输出：
DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
           10 Administration                 4848
           20 Marketing                      19896
           30 Purchasing                     27588
           40 Human Resources                6948
           50 Shipping                       176560

过程Get_Employees()测试代码：
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/

输出：
101 Neena
    108 Nancy
        109 Daniel
        110 John
        111 Ismael
        112 Jose Manuel
        113 Luis
    200 Jennifer
    203 Susan
    204 Hermann
    205 Shelley
        206 William
```

# 总结

这个实验旨在练习构建程序包，使用递归查询语句查询员工及其下属、子下属员工的信息，并使用游标和 oracle过程实现。实验中了两个不同的查询语句，分别查询员工的 ID、名字和姓氏信息，以及员工的层级、ID 和名字信息。通过实验，学生可以掌握如何使用递归查询语句查询树形结构数据，并使用游标和 oracle 过程将查询结果输出。
